# Language basics

## Learning outcomes
* working with IntelliJ 
* getting to know Java coding conventions 
* some basic OO programming

## Assignment details
Simply work your way through the methods and implement them according to the instructions 
stated within the methods' Javadoc or in the method body 

**Please note that is it absolutely essential that you use the given method stubs to 
complete the assignment!**